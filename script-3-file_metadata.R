require(tuneR)

df_samples <- rbind(
  cbind(site = "m27", read.csv("./recordings_site-1_m27.csv")),
  cbind(site = "mansbridge", read.csv("./recordings_site-2_mansbridge.csv")),
  cbind(site = "woodmill", read.csv("./recordings_site-3_woodmill.csv")))

df_samples$file_size = NA
df_samples$duration = NA

for(i in 1:nrow(df_samples)){

  recorder <- df_samples$recorder[i]

  # ------ Get file path
  if(recorder == "ZOOM"){
    path_abs <- with(
      df_samples,
      sprintf("./%s/%s/%s_Tr1.WAV",
              file_path[i], recording_name[i], recording_name[i]))
  }else{
    path_abs <- with(
      df_samples,
      sprintf("./%s/%s",
              file_path[i], recording_name[i]))
  }

  suppressWarnings({
    info <- readWave(path_abs, header = T)
  })


  df_samples$duration[i] <- info$samples / info$sample.rate
  df_samples$file_size[i] <- info$samples * (info$bits/8) * info$channels / (1e6)

}

df_samples$duration <- round(df_samples$duration, 1)
df_samples$file_size <- round(df_samples$file_size, 1)

write.csv(file = "./results/3-wav_metadata.csv", x = df_samples)
