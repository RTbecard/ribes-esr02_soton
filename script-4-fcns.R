require(signal)
require(fftw)
require(tictoc)

decidecade_bins <- function(sample_rate){

  f_nyq <- sample_rate / 2
  decidecade_max <- floor(10*log10(f_nyq) - 0.5)
  decidecade_min <- ceiling(10*log10(10) + 0.5)

  df_decidecades <- data.frame(i = decidecade_min:decidecade_max)
  df_decidecades$center <- 10^(df_decidecades$i/10)
  df_decidecades$min <- 10^((df_decidecades$i - 0.5)/10)
  df_decidecades$max <- 10^((df_decidecades$i + 0.5)/10)

  return(df_decidecades)
}

binned_analysis <- function(wav_fs, bin_period = 1, olap = 0.5){

  bin_starts <- seq(0,end(wav_fs)[1] - bin_period, bin_period*olap)
  n <- length(bin_starts)

  df_bin_decidecade <- data.frame()
  df_bin_meansquare <- data.frame()

  f_nyq <- frequency(wav_fs)/2
  df_decidecades <- decidecade_bins(frequency(wav_fs))

  vec_rms <- vector(mode = "numeric", length = n)
  mat_decidecades <- matrix(NA, nrow = n, ncol = nrow(df_decidecades))

  # Init dataframe for holding saved decidecade values
  # get frequency values
  freq <- seq(0, frequency(wav_fs), length.out = bin_period*frequency(wav_fs))
  # Extract single sided, get complex magnitude, & convert to power
  idx_single <- which(freq <= frequency(wav_fs) / 2)
  df_pow <- data.frame(
    power = vector(mode = 'numeric', length = length(idx_single)),
    freq = freq[idx_single])
  # Group decidecade frequencies
  df_pow$decidecade = as.integer(cut(df_pow$freq, breaks = c(df_decidecades$min[1], df_decidecades$max)))

  # Loop bins
  for(i in 1:length(bin_starts)){

    # Clip bin period from wav file
    clip <- ts(window(wav_fs, bin_starts[i], bin_starts[i] + bin_period)[-1], frequency = frequency(wav_fs))

    # Calculate mean square pressure
    vec_rms[i] <- 20*(log10(sqrt(mean((clip) ^2))))

    # Calculate decidecades
    # we'll convert to frequency domain, then
    wndw <- signal::hanning(length(clip))  # apply hanning window
    corr = 1 / sqrt(mean(wndw^2))  # energy correction factor

    # tic()
    clip_fft <- fftw::FFT(clip * wndw) * corr / length(clip) # apply fft
    # toc()

    # Get power spectrum
    # tic()
    df_pow$power <-  (abs(clip_fft[idx_single])*2)^2
    # toc()

    # Return decidecade mean square pressure
    # tic()
    mat_decidecades[i,] <- 10*log10(aggregate(power ~ decidecade, sum, data = df_pow)[,2])
    # toc()

    # verify Decidecade values
    # The sum of the decidecades should be the same as the rms measure
    # Note the rms measure will differ because it is not windowed
    # 10*log10(sum(10^(mat_decidecades[i,]/10)))
  }
  return(list(
    rms = data.frame(bin = 1:length(vec_rms), rms = vec_rms),
    decidecade = data.frame(
      bin = 1:length(vec_rms),
      cntr_freq = round(df_decidecades$center, 0),
      L = as.vector(t(mat_decidecades)))))
}
